package com.example.tilseroz.jokegenerator;

import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Rect;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.TypedValue;
import android.view.View;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;
import rx.Observable;
import rx.Observer;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

public class MainActivity extends AppCompatActivity {

    @BindView(R.id.recyclerview) RecyclerView recyclerView;
    List<Pojo> list;
    public static final String JOKEMESSAGE = "com.example.tilseroz.jokegenerator.MESSAGE";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this, this);

        getProductData();
    }

    public void showDetail(View view) {
        Intent intent = new Intent(this, DetailActivity.class);
        TextView textView = findViewById(R.id.setup);
        String message = textView.getText().toString();
        intent.putExtra(JOKEMESSAGE, message);
        startActivity(intent);
    }


    public void getProductData(){

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("https://08ad1pao69.execute-api.us-east-1.amazonaws.com")
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                .build();
        APIService apiService = retrofit.create(APIService.class);

        Observable<List<Pojo>> observable = apiService.getSetup().subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread());
        observable.subscribe(new Observer<List<Pojo>>() {
            @Override
            public void onCompleted() {

            }

            @Override
            public void onError(Throwable e) {

            }

            @Override
            public void onNext(List<Pojo> jokes) {

                list = new ArrayList<>();
                for (int i =0;i<jokes.size();i++){

                    Pojo pojo = new Pojo();
                    pojo.setSetup(jokes.get(i).getSetup());
                    pojo.setPunchline(jokes.get(i).getPunchline());
                    list.add(pojo);
                }

                MyAdapter recyclerAdapter = new MyAdapter(list);
                RecyclerView.LayoutManager recyce = new GridLayoutManager(MainActivity.this,2);
                recyclerView.addItemDecoration(new GridSpacingDecoration(2, dpToPx(10), true));
                recyclerView.setLayoutManager(recyce);
                recyclerView.setAdapter(recyclerAdapter);
            }
        });
    }


    public class GridSpacingDecoration extends RecyclerView.ItemDecoration {

        private int span;
        private int space;
        private boolean include;

        private GridSpacingDecoration(int span, int space, boolean include) {
            this.span = span;
            this.space = space;
            this.include = include;
        }

        @Override
        public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
            int position = parent.getChildAdapterPosition(view);
            int column = position % span;

            if (include) {
                outRect.left = space - column * space / span;
                outRect.right = (column + 1) * space / span;

                if (position < span) {
                    outRect.top = space;
                }
                outRect.bottom = space;
            } else {
                outRect.left = column * space / span;
                outRect.right = space - (column + 1) * space / span;
                if (position >= span) {
                    outRect.top = space;
                }
            }
        }

    }

    private int dpToPx(int dp) {
        Resources r = getResources();
        return Math.round(TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp, r.getDisplayMetrics()));
    }
}
