package com.example.tilseroz.jokegenerator;

/**
 * Created by Tilseroz on 29. 10. 2017.
 */

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MyAdapter extends RecyclerView.Adapter<MyAdapter.Myholder> {
    List<Pojo> list;

    public MyAdapter(List<Pojo> list) {
        this.list = list;
    }

    @Override
    public Myholder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.show, parent,false);
        Myholder myHolder = new Myholder(view);
        return myHolder;
    }

    @Override
    public void onBindViewHolder(Myholder holder, int position) {

        Pojo pojo = list.get(position);
        holder.setup.setText(pojo.getSetup() + " " + pojo.getPunchline());
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class Myholder extends RecyclerView.ViewHolder{
        @BindView(R.id.setup)
        TextView setup;

        public Myholder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

}
