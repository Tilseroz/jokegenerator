package com.example.tilseroz.jokegenerator;

/**
 * Created by Tilseroz on 29. 10. 2017.
 */

import java.util.List;

import retrofit2.http.GET;
import rx.Observable;

public interface APIService {

    @GET("/dev/random_ten")
    Observable<List<Pojo>> getSetup();

}
